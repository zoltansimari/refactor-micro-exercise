﻿using TurnTicketDispenser.Domain;

namespace TurnTicketDispenser.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependencies on 
    /// TurnNumberSequence and TurnTicket that have impact on the refactoring.
    /// </summary>
    public class TurnTicketAndSequenceClient
    {
        public TurnTicketAndSequenceClient()
        {
            var turnTicket1 = new TurnTicket(TurnNumberSequence.GetNextTurnNumber());
            var turnTicket2 = new TurnTicket(TurnNumberSequence.GetNextTurnNumber());
            var turnTicket3 = new TurnTicket(TurnNumberSequence.GetNextTurnNumber());
        }
    }
}
