﻿using TurnTicketDispenser.Domain;

namespace TurnTicketDispenser.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on TurnNumberSequence
    /// that has impact on the refactoring.
    /// </summary>
    public class TurnNumberSequenceClient
    {
        public TurnNumberSequenceClient()
        {
            int nextUniqueTicketNumber;
            nextUniqueTicketNumber = TurnNumberSequence.GetNextTurnNumber();
            nextUniqueTicketNumber = TurnNumberSequence.GetNextTurnNumber();
            nextUniqueTicketNumber = TurnNumberSequence.GetNextTurnNumber();
        }
    }
}
