﻿using TurnTicketDispenser.Domain;

namespace TurnTicketDispenser.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on TicketDispenser
    /// that has impact on the refactoring.
    /// </summary>
    public class TicketDispenserClient
    {
        public TicketDispenserClient()
        {
            new TicketDispenser().GetTurnTicket();
            new TicketDispenser().GetTurnTicket();
            new TicketDispenser().GetTurnTicket();
        }
    }
}
