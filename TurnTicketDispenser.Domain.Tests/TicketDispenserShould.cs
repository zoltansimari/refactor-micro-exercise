using FluentAssertions;
using NUnit.Framework;
using System.Linq;

namespace TurnTicketDispenser.Domain.Tests
{
    public class TicketDispenserShould
    {
        [Test]
        public void ShouldReturnUniqueTickets()
        {
            var dispenser = new TicketDispenser(new FakeSequenceProvider());
            var result = Enumerable.Repeat(string.Empty, 100).Select(x => dispenser.GetTurnTicket());
            result.Should().OnlyHaveUniqueItems();
        }
    }

    internal class FakeSequenceProvider : ISequenceProvider
    {
        private int _number = 0;
        public int GetNext() => _number++;
    }
}