﻿namespace TurnTicketDispenser.Domain
{
    public class SequenceProvider : ISequenceProvider
    {
        public int GetNext() => TurnNumberSequence.GetNextTurnNumber();
    }
}
