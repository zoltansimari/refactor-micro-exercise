namespace TurnTicketDispenser.Domain
{
    public class TurnTicket
    {
        public TurnTicket(int turnNumber)
        {
            TurnNumber = turnNumber;
        }

        public int TurnNumber { get; }
    }
}