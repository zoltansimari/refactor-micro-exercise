﻿namespace TurnTicketDispenser.Domain
{
    public interface ISequenceProvider
    {
        int GetNext();
    }
}
