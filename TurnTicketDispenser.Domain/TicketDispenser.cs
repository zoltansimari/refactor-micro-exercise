namespace TurnTicketDispenser.Domain
{
    public class TicketDispenser
    {
        private readonly ISequenceProvider _sequenceProvider;

        // TODO: Refactor to remove this constructor after removing all usage
        public TicketDispenser()
        {
            _sequenceProvider = new SequenceProvider();
        }

        public TicketDispenser(ISequenceProvider sequenceProvider)
        {
            _sequenceProvider = sequenceProvider;
        }

        public TurnTicket GetTurnTicket()
        {
            int currentNumber = _sequenceProvider.GetNext();
            return new TurnTicket(currentNumber);
        }
    }
}
