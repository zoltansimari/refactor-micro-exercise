﻿using TirePressureMonitoring.Domain;

namespace TirePressureMonitoring.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on Alert
    /// that has impact on the refactoring.
    /// </summary>
    public class AnAlarmClient1
    {
        public AnAlarmClient1()
        {
            Alarm anAlarm = new Alarm();
            anAlarm.Check();
            bool isAlarmOn = anAlarm.AlarmOn;
        }
    }
}
