﻿using TirePressureMonitoring.Domain;

namespace TirePressureMonitoring.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on Sensor
    /// that has impact on the refactoring.
    /// </summary>
    public class ASensorClient
    {
        public ASensorClient()
        {
            Sensor sensor = new Sensor();

            double value = sensor.PopNextPressurePsiValue();
            value = sensor.PopNextPressurePsiValue();
            value = sensor.PopNextPressurePsiValue();
            value = sensor.PopNextPressurePsiValue();
        }
    }
}
