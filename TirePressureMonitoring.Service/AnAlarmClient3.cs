﻿using TirePressureMonitoring.Domain;

namespace TirePressureMonitoring.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on Alert
    /// that has impact on the refactoring.
    /// </summary>
    public class AnAlarmClient3
    {
        private Alarm _anAlarm;

        public AnAlarmClient3()
        {
            _anAlarm = new Alarm();
        }

        public void DoSomething()
        {
            _anAlarm.Check();
        }

        public void DoSomethingElse()
        {
            bool isAlarmOn = _anAlarm.AlarmOn;
        }
    }
}
