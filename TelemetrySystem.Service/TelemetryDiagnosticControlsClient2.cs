﻿using TelemetrySystem.Domain;

namespace TelemetrySystem.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on TelemetryDiagnosticControls
    /// that has impact on the refactoring.
    /// </summary>
    public class TelemetryDiagnosticControlsClient2
    {
        public TelemetryDiagnosticControlsClient2()
        {
            var teleDiagnostic = new TelemetryDiagnosticControls();

            teleDiagnostic.CheckTransmission();

            var result = teleDiagnostic.DiagnosticInfo;
        }
    }
}
