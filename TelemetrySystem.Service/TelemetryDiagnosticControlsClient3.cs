﻿using TelemetrySystem.Domain;

namespace TelemetrySystem.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on TelemetryDiagnosticControls
    /// that has impact on the refactoring.
    /// </summary>
    public class TelemetryDiagnosticControlsClient3
    {
        public TelemetryDiagnosticControlsClient3()
        {
            var teleDiagnostic = new TelemetryDiagnosticControls();

            teleDiagnostic.CheckTransmission();

            var result = teleDiagnostic.DiagnosticInfo;
        }
    }
}
