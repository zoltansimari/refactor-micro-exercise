﻿using TelemetrySystem.Domain;

namespace TelemetrySystem.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on TelemetryClient
    /// that has impact on the refactoring.
    /// </summary>
    public class TelemetryClientClient
    {
        public TelemetryClientClient()
        {
            var tc = new TelemetryClient();
            if (!tc.OnlineStatus)
                tc.Connect("a connection string");

            tc.Send("some message");

            var response = tc.Receive();

            tc.Disconnect();
        }
    }
}
