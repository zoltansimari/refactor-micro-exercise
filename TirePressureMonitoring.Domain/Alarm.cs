namespace TirePressureMonitoring.Domain
{
    public class Alarm
    {
        // TODO: Make range configurable by passing it in the constructor
        private const double LowPressureThreshold = 17;
        private const double HighPressureThreshold = 21;

        readonly ISensor _sensor;

        // TODO: Refactor code to remove usage of parameterless constructor
        public Alarm()
        {
            _sensor = new Sensor();
        }

        public Alarm(ISensor sensor)
        {
            _sensor = sensor;
        }

        public void Check()
        {
            double psiPressureValue = _sensor.PopNextPressurePsiValue();

            if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue)
            {
                AlarmOn = true;
            }
        }

        public bool AlarmOn { get; private set; } = false;
    }
}
