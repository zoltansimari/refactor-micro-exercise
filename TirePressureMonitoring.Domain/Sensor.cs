using System;

namespace TirePressureMonitoring.Domain
{
    /// <summary>
    /// The reading of the pressure value from the sensor is simulated in this implementation.
    /// Because the focus of the exercise is on the other class.
    /// </summary>
    public class Sensor : ISensor
    {
        const double Offset = 16;
        readonly Random _randomPressureSampleSimulator = new Random();

        public double PopNextPressurePsiValue()
        {
            double pressureTelemetryValue = ReadPressureSample();

            return Offset + pressureTelemetryValue;
        }

        private double ReadPressureSample()
        {
            // Simulate info read from a real sensor in a real tire
            return 6 * _randomPressureSampleSimulator.NextDouble() * _randomPressureSampleSimulator.NextDouble();
        }
    }
}
