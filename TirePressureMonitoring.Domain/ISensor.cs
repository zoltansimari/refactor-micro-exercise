﻿namespace TirePressureMonitoring.Domain
{
    public interface ISensor
    {
        double PopNextPressurePsiValue();
    }
}