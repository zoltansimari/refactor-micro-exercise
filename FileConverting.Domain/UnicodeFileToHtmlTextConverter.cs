using System.Text;
using System.Web;

namespace FileConverting.Domain
{
    public class UnicodeFileToHtmlTextConverter
    {
        private readonly string _fullFilenameWithPath;
        private readonly IFileHandler _fileHandler;
        private static readonly string HtmlBreakLine = "<br />";

        // TODO: Remove this constructor after all usages refactored
        public UnicodeFileToHtmlTextConverter(string fullFilenameWithPath)
        {
            _fullFilenameWithPath = fullFilenameWithPath;
            _fileHandler = new FileHandler();
        }

        // TODO: Remove full path as a constructor parameter 
        public UnicodeFileToHtmlTextConverter(string fullFilenameWithPath, IFileHandler fileHandler)
        {
            _fullFilenameWithPath = fullFilenameWithPath;
            _fileHandler = fileHandler;
        }

        public string ConvertToHtml()
        {
            var output = new StringBuilder();
            var fileContentLines = _fileHandler.ReadAllLines(_fullFilenameWithPath);
            foreach (var line in fileContentLines)
            {
                output.Append(HttpUtility.HtmlEncode(line));
                // TODO: Confirm whether the content needs to always end with the HTML break line 
                output.Append(HtmlBreakLine);
            }

            return output.ToString();
        }
    }
}
