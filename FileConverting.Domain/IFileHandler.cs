﻿using System.Collections.Generic;

namespace FileConverting.Domain
{
    public interface IFileHandler
    {
        IEnumerable<string> ReadAllLines(string filePath);
    }
}
