﻿using System.Collections.Generic;
using System.IO;

namespace FileConverting.Domain
{
    public class FileHandler : IFileHandler
    {
        public IEnumerable<string> ReadAllLines(string filePath) => File.ReadAllLines(filePath);
    }
}
