using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;

namespace FileConverting.Domain.Tests
{
    public class UnicodeFileToHtmlTextConverterShould
    {
        private Mock<IFileHandler> _fileHandlerMock;

        [SetUp]
        public void Setup()
        {
            _fileHandlerMock = new Mock<IFileHandler>();
        }

        [Test]
        [TestCase("a", "a<br />")]
        [TestCase("a\r\nb", "a<br />b<br />")]
        [TestCase("a > b", "a &gt; b<br />")]
        public void ShouldHtmlEncode(string input, string expectedOutput)
        {
            _fileHandlerMock.Setup(fh => fh.ReadAllLines(It.IsAny<string>())).Returns(input.Split(Environment.NewLine));
            var converter = new UnicodeFileToHtmlTextConverter(string.Empty, _fileHandlerMock.Object);
            var html = converter.ConvertToHtml();

            _fileHandlerMock.Verify(mock => mock.ReadAllLines(It.IsAny<string>()), Times.Once());
            html.Should().Be(expectedOutput);
        }
    }
}