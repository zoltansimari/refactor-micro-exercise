﻿using FluentAssertions;
using NUnit.Framework;

namespace TirePressureMonitoring.Domain.Tests
{
    public class SensorShould
    {
        [Test]
        [Repeat(100)]
        public void ShouldBeInRange()
        {
            var fakeSensor = new Sensor();
            fakeSensor.PopNextPressurePsiValue().Should().BeGreaterOrEqualTo(16).And.BeLessOrEqualTo(22);
        }
    }
}
