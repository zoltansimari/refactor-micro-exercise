using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace TirePressureMonitoring.Domain.Tests
{
    public class AlarmShould
    {
        private Mock<ISensor> _sensorMock;

        [SetUp]
        public void Setup()
        {
            _sensorMock = new Mock<ISensor>();
        }

        [Test]
        [TestCase(16.99, true)]
        [TestCase(17, false)]
        [TestCase(17.01, false)]
        [TestCase(20.99, false)]
        [TestCase(21, false)]
        [TestCase(21.01, true)]
        public void ShouldGetMeasurementAndSetAlarm(double measuredPsiValue, bool isAlarmExpectedTobeOn)
        {
            _sensorMock.Setup(sm => sm.PopNextPressurePsiValue()).Returns(measuredPsiValue);
            var alarm = new Alarm(_sensorMock.Object);
            alarm.Check();

            _sensorMock.Verify(mock => mock.PopNextPressurePsiValue(), Times.Once());
            alarm.AlarmOn.Should().Be(isAlarmExpectedTobeOn);
        }
    }
}