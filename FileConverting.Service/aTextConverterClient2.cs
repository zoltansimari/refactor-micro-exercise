﻿using FileConverting.Domain;

namespace FileConverting.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on UnicodeFileToHtmTextConverter
    /// that has impact on the refactoring.
    /// </summary>
    public class aTextConverterClient2
    {
        UnicodeFileToHtmlTextConverter _textConverter;

        public aTextConverterClient2()
        {
            _textConverter = new UnicodeFileToHtmlTextConverter("anotherFilename.txt");

            var html = _textConverter.ConvertToHtml();
        }
    }
}
