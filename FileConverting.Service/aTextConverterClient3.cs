﻿using System;

namespace FileConverting.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on UnicodeFileToHtmTextConverter
    /// that has impact on the refactoring.
    /// </summary>
    public class aTextConverterClient3
    {
        public aTextConverterClient3()
        {
            object[] args = { "jetAnotherFilename.txt" };
            
            // TODO: Remove Activator usage
            // Note: Alternative to editing the string below is to have type sitting under the original namespace 
            // (inheriting from the new class without implementation/changes), but it is not recomended because the risk it imposes
            dynamic x = Activator.CreateInstance(Type.GetType("FileConverting.Domain.UnicodeFileToHtmTextConverter"), args);  

            string html = x.ConvertToHtml();
        }
    }
}
