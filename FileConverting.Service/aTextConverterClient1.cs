﻿using FileConverting.Domain;

namespace FileConverting.Service
{
    /// <summary>
    /// A class with the only goal of simulating a dependency on UnicodeFileToHtmTextConverter
    /// that has impact on the refactoring.
    /// </summary>
    public class aTextConverterClient1
    {
        public aTextConverterClient1()
        {
            var filename = "aFilename.txt";
            var textConverter = new UnicodeFileToHtmlTextConverter(filename);
            var html = textConverter.ConvertToHtml();
        }
    }
}
